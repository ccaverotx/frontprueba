export default async function getAllNews(){
    const res = await fetch('http://localhost:3000/news')

    if(!res.ok) throw new Error('failed to fetch data')

    return res.json()
}