export default async function getNews(newsId: number) {

    const res = await fetch(`http://localhost:3000/news/${newsId}`)

    if(!res.ok) throw new Error('Failed to fetch user')

    return res.json()
}