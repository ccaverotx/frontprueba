type News = {
    "id": number,
    "name": string,
    "date": Date,
    "place": string,
    "author": string,
    "description": string,
    "active": boolean
}