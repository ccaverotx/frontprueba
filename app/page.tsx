import styles from './page.module.css'
import News from "@/app/news/page";
import Header from '@/app/components/header/header';
export default function Home() {
  return (
    <main className={styles.main}>
      <Header />
      <News />
    </main>
  )
}
