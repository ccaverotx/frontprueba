import Header from "@/app/components/header/header";
import styles from "@/app/news/[newsId]/inew.module.css";
import Box from "@mui/material/Box";
import {TextField} from "@mui/material";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

export default async function addNews(){
    return(
        <>
        <Header/>
            <main className={styles.main}>
                <Box
                    component="form"
                    sx={{
                        '& .MuiTextField-root': { m: 1, width: '25ch' },
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <Typography variant="h2" gutterBottom>
                        Inserte los detalles de la nueva noticia:
                    </Typography>
                    <div>
                        <TextField
                            id="outlined-multiline-flexible"
                            label="Título"
                            multiline
                            maxRows={4}
                        />
                        <TextField
                            id="outlined-multiline-flexible"
                            label="Lugar"
                            multiline
                            maxRows={4}
                        />
                        <TextField
                            id="outlined-multiline-flexible"
                            label="Autor"
                            multiline
                            maxRows={4}
                        />
                        <TextField
                            id="outlined-multiline-flexible"
                            label="Descripción"
                            multiline
                            maxRows={4}
                        />
                    </div>
                    <Button variant="contained">Guardar</Button>
                </Box>
            </main>
        </>
    )
}