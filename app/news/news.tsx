'use client'
import {ButtonBase, Grid, Paper} from "@mui/material";
import scss from './news.module.scss';
import getAllNews from "@/lib/getAllNews";
import Link from "next/link";
import { styled } from '@mui/material/styles';
import {Metadata} from "next";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";


export const metadata: Metadata = {
    title: 'News',
}
const Img = styled('img')({
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
});
// eslint-disable-next-line @next/next/no-async-client-component
export default async function NewsPage(){
    const newsData: Promise<News[]> = getAllNews()
    const news = await newsData
    console.log('Hello')
    return (
        <section>
            <br/>
            <Button variant="contained" href={'/news/addNews'}>Crear Noticia</Button>
            {news.map(news => {
                return (
                    // eslint-disable-next-line react/jsx-key
                    <Paper  className={scss.dataCard}
                        sx={{
                            p: 2,
                            margin: 'auto',
                            maxWidth: 500,
                            flexGrow: 1,
                            backgroundColor: (theme) =>
                                theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
                        }}
                    >
                        <Grid container spacing={{ md: 5}} columns={{ xs: 4, sm: 8, md: 12 }}>
                            <Grid item>
                                {/* eslint-disable-next-line react/jsx-no-undef */}
                                <ButtonBase sx={{ width: 128, height: 128 }}>
                                    <Img alt="complex" src="https://static.toiimg.com/thumb/imgsize-21670,msid-91113212,width-400,resizemode-4/91113212.jpg" />
                                </ButtonBase>
                            </Grid>
                            <Grid item xs={12} sm container>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        {/* eslint-disable-next-line react/jsx-no-undef */}
                                        <Typography gutterBottom variant="subtitle1" component="div">
                                            <Link href={`/news/${news.id}`}>{news.name}</Link>
                                        </Typography>
                                        <Typography variant="body2" gutterBottom>
                                            {news.author}
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            {news.date.toString()}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                            {news.place}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Typography variant="subtitle1" component="div">
                                        {news.id}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                )
            })}
        </section>
    )
}