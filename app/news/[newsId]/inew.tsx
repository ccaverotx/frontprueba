import styles from './inew.module.css'
import getNews from "@/lib/getNews";
import Header from "@/app/components/header/header";
import {Card, CardActions, CardContent, CardHeader, CardMedia, Grid} from "@mui/material";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import scss from "@/app/news/news.module.scss";
import IconButton from "@mui/material/IconButton";
import MoreVertIcon from '@mui/icons-material/MoreVert';

type Params = {
    params: {
        newsId: number
    }
}
export default async function NewsPage({params: {newsId}}:Params){
    const newsData: Promise<News> = getNews(newsId)
    const [news] = await Promise.all([newsData])
    return(
        <><Header/>
            <main className={styles.main}>
                <Grid className={scss.dataCard}>
                    <Card sx={{maxWidth: 1500}}>
                        <CardHeader
                            action={
                                <IconButton aria-label="settings">
                                    <MoreVertIcon />
                                </IconButton>
                            }
                            title={news.name}
                            subheader={news.date.toString()
                        }
                        />
                        <CardMedia
                            sx={{height: 400}}
                            image="https://static.toiimg.com/thumb/imgsize-21670,msid-91113212,width-400,resizemode-4/91113212.jpg"
                            title="breaking news"/>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                {news.description}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {news.place}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button size="small">Share</Button>
                            <Button size="small">{news.author}</Button>
                        </CardActions>
                    </Card>
                </Grid>
            </main>
            </>
    )
}